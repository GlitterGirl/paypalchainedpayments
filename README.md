# Aurelian PayPal Chained Payments

The Aurelian Paypal Chained Payments package provides a hassle-free method of executing
chained payments through the PayPal API for your Symfony Application. 

* Simple Configuration
* PHP object responses
* Command line interface

## Installation
The preferred method of installation is via [Composer](https://getcomposer.org). Run the following command to install the
package and add it as a requirement in your project's `composer.json`:

```bash
composer require aurelian/paypal-chained-payments
```

